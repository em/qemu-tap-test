# Run two QEMU VMs sharing a private network

Set up some kind of bridged connection point two QEMU instances to use it to
ping each other.

Three scripts are provided:

* `qemu-tap-test-rootful.sh`: set up the TAP devices on the host, requiring
  `root` access via `sudo`
* `qemu-tap-test-rootless.sh`: set up the TAP devices in a container,
  without requiring `root` access
* `qemu-tap-test-rootless-vde.sh`: set up a VDE switch in a container,
  completely avoiding TAP devices

They expect a QCOW2 VM image running Ignition and systemd such as
[`fedora-coreos-39.20240112.3.0-qemu.x86_64.qcow2`](https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/39.20240112.3.0/x86_64/fedora-coreos-39.20240112.3.0-qemu.x86_64.qcow2.xz).

The Ignition provisioning instructions takes care of:

* configuring basic networking on the TAP interface
* replacing the login on the terminal with `ping` testing the connectivity
  to the other VM:

```
[  OK  ] Reached target multi-user.target - Multi-User System.
         Starting systemd-update-utmp-runle…- Record Runlevel Change in UTMP...
         Starting zincati.service - Zincati Update Agent...
[  OK  ] Finished systemd-update-utmp-runle…e - Record Runlevel Change in UTMP.
         Starting rpm-ostreed.service - rpm-ostree System Management Daemon...
[  OK  ] Started rpm-ostreed.service - rpm-ostree System Management Daemon.
         Starting polkit.service - Authorization Manager...
[  OK  ] Started polkit.service - Authorization Manager.
[  OK  ] Started zincati.service - Zincati Update Agent.

===============
Connection test
===============

2: ens4: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:12:34:56 brd ff:ff:ff:ff:ff:ff
    altname enp0s4
    inet 10.0.0.2/24 brd 10.0.0.255 scope global noprefixroute ens4
       valid_lft forever preferred_lft forever
    inet6 fe80::1544:4448:f671:4c79/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever

PING 10.0.0.3 (10.0.0.3) 56(84) bytes of data.
64 bytes from 10.0.0.3: icmp_seq=1 ttl=64 time=0.511 ms
64 bytes from 10.0.0.3: icmp_seq=2 ttl=64 time=1.44 ms
64 bytes from 10.0.0.3: icmp_seq=3 ttl=64 time=1.35 ms
64 bytes from 10.0.0.3: icmp_seq=4 ttl=64 time=1.48 ms
64 bytes from 10.0.0.3: icmp_seq=5 ttl=64 time=1.26 ms

--- 10.0.0.3 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4008ms
rtt min/avg/max/mdev = 0.511/1.208/1.484/0.357 ms
[root@localhost /]# 
```
