#!/bin/bash
#
# Run two QEMU VMs sharing a private network based on TAP devices
# using network namespaces to avoid needing root

set -eux

IMG=$1
IMG=$(readlink -f "$IMG")

IMG_FORMAT=$(qemu-img info "$IMG" --output json | jq -r '.["format-specific"].type')
if [ "$IMG_FORMAT" != qcow2 ]
then
	echo "The VM image needs to be in QCOW2 format"
	exit 1
fi

setup-tap() {
	echo "== Set up the tap devices in a network namespace"

	# this would be much easier if iproute2 did not hardcode "/var/run/netns"
	# and let us use "/run/user/$(id -u)/netns" instead

	# from https://wiki.qemu.org/Documentation/Networking#Setting_up_taps_on_Linux

	podman run -i --rm --init --privileged --entrypoint= qemux/qemu-docker bash -c "
  set -uex
	ip link add qabr0 type bridge
	ip tuntap add dev qatap0 mode tap
	ip tuntap add dev qatap1 mode tap
	ip link set dev qatap0 master qabr0
	ip link set dev qatap1 master qabr0
	ip link set dev qabr0 up
	ip link set dev qatap0 up
	ip link set dev qatap1 up
  sleep 1h # keep the container alive
  " &
}

setup-tap
sleep 1 # give time for podman to start, we need to capture the container id

NET_CONTAINER_ID=$(podman ps -l --format '{{.ID}}')
echo $NET_CONTAINER_ID

PASSWORD_HASH=$(echo -n test | mkpasswd --method=yescrypt --stdin)

runvm() {
	HOSTNAME=$1
	TAP=$2
	IPADDR=$3
	MACADDR=$4
	REMOTEIPADDR=$5
	SERIAL=$6

	#GETTY=getty@tty1         # VT when using -display=gtk
	GETTY=serial-getty@ttyS0 # serial console when using -serial=stdio

	echo "== Build the provisioning script for the $HOSTNAME VM, to automatically ping the other VM"
	podman run --interactive --rm quay.io/coreos/butane:release --pretty --strict >"/tmp/$HOSTNAME.ign" <<EOF
variant: fcos
version: 1.5.0
passwd:
  users:
    - name: testuser
      password_hash: "$PASSWORD_HASH"
storage:
  files:
    - path: /etc/NetworkManager/system-connections/ens4.nmconnection
      mode: 0600
      contents:
        inline: |
          [connection]
          id=ens4
          type=ethernet
          interface-name=ens4
          [ipv4]
          address1=$IPADDR/24,10.10.10.1
          dns=
          dns-search=
          may-fail=false
          method=manual
    - path: /etc/systemd/system/$GETTY.service.d/override.conf
      mode: 0600
      contents:
        inline: |
          [Service]
          # grant the ability to run ip
          SELinuxContext=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023
          ExecStart=
          ExecStart=/usr/bin/bash -c 'printf "\n===============\nConnection test\n===============\n\n"; ip addr list dev ens4; echo; ping -c 5 $REMOTEIPADDR; exec bash --login'
          Type=idle
EOF
	rm -f "/tmp/$HOSTNAME.qcow2"
	qemu-img create -f qcow2 -b "$IMG" -F qcow2 "/tmp/$HOSTNAME.qcow2"

	echo "== Run the $HOSTNAME VM, joining the network namespace from container $NET_CONTAINER_ID"
	podman run \
		--rm \
		-it \
		--init \
		--network "container:$NET_CONTAINER_ID" \
		--security-opt label=disable \
		-v /tmp:/tmp \
		-v "$IMG:$IMG" \
		--device /dev/kvm \
		--device /dev/net/tun \
		--entrypoint= \
		qemux/qemu-docker \
		qemu-system-x86_64 \
		-name "$HOSTNAME @$IPADDR" \
		-nodefaults \
		-serial "$SERIAL" \
		-m 512M \
		-enable-kvm \
		-cpu host \
		-vga virtio \
		-display none \
		-fw_cfg name=opt/com.coreos/config,file="/tmp/$HOSTNAME.ign" \
		-device virtio-rng-pci \
		-netdev tap,id=qanet0,ifname=$TAP,script=no,downscript=no \
		-device "virtio-net,netdev=qanet0,mac=$MACADDR" \
		-drive "if=virtio,format=qcow2,file=/tmp/$HOSTNAME.qcow2"
}

runvm host2 qatap1 10.0.0.3 52:54:00:12:34:57 10.0.0.2 none &
runvm host1 qatap0 10.0.0.2 52:54:00:12:34:56 10.0.0.3 stdio

wait $(jobs -p)
