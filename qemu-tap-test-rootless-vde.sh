#!/bin/bash
#
# Run two QEMU VMs sharing a private network based on VDE devices
# using SLIRP to provide access to the Internet

set -eux

IMG=$1
IMG=$(readlink -f "$IMG")

IMG_FORMAT=$(qemu-img info "$IMG" --output json | jq -r '.["format-specific"].type')
if [ "$IMG_FORMAT" != qcow2 ]; then
	echo "The VM image needs to be in QCOW2 format"
	exit 1
fi

podman rm --force qemu-tap-test-rootless-vde-switch || true
podman rm --force qemu-tap-test-rootless-vde-host1 || true
podman rm --force qemu-tap-test-rootless-vde-host2 || true

setup-switch() {
	echo "== Set up the vde switch"

	rm /tmp/qa-switch -rf
	mkdir /tmp/qa-switch

	podman run --name qemu-tap-test-rootless-vde-switch -i --rm --init --security-opt label=disable -v "/tmp/qa-switch":/qa-switch --detach debian:bookworm-slim bash -c "
  set -uex
  apt-get update
  apt-get install -y --no-install-recommends vde2
  vde_switch --sock /qa-switch/vde --daemon
  slirpvde --sock /qa-switch/vde --host 10.10.10.10/8 --dns 10.10.10.11
  "

	while [ ! -e /tmp/qa-switch/vde/ctl ]; do
		sleep 1
	done
}

setup-switch

PASSWORD_HASH=$(echo -n test | mkpasswd --method=yescrypt --stdin)

runvm() {
	HOSTNAME=$1
	shift
	IPADDR=$1
	shift
	MACADDR=$1
	shift
	REMOTEIPADDR=$1
	shift

	#GETTY=getty@tty1         # VT when using -display=gtk
	GETTY=serial-getty@ttyS0 # serial console when using -serial=stdio

	echo "== Build the provisioning script for the $HOSTNAME VM, to automatically ping the other VM"
	podman run --interactive --rm quay.io/coreos/butane:release --pretty --strict >"/tmp/$HOSTNAME.ign" <<EOF
variant: fcos
version: 1.5.0
passwd:
  users:
    - name: testuser
      password_hash: "$PASSWORD_HASH"
storage:
  files:
    - path: /etc/NetworkManager/system-connections/ens4.nmconnection
      mode: 0600
      contents:
        inline: |
          [connection]
          id=ens4
          type=ethernet
          interface-name=ens4
          [ipv4]
          address1=$IPADDR/8,10.10.10.10
          dns=10.10.10.11
          dns-search=
          may-fail=false
          method=manual
    - path: /etc/systemd/system/$GETTY.service.d/override.conf
      mode: 0600
      contents:
        inline: |
          [Service]
          # grant the ability to run ip
          SELinuxContext=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023
          ExecStart=
          ExecStart=/usr/bin/bash -c 'printf "\n===============\nConnection test\n===============\n\n"; set -ux; ip addr list dev ens4; echo; ping -c 5 $REMOTEIPADDR; curl collabora.com -svI -o /dev/null --connect-timeout 120; exec bash --login'
          Type=idle
EOF
	rm -f "/tmp/$HOSTNAME.qcow2"
	qemu-img create -f qcow2 -b "$IMG" -F qcow2 "/tmp/$HOSTNAME.qcow2"

	echo "== Run the $HOSTNAME VM"
	podman run \
		--name qemu-tap-test-rootless-vde-$HOSTNAME \
		--rm \
		-it \
		--init \
		--security-opt label=disable \
		-v "/tmp/qa-switch:/qa-switch" \
		-v /tmp:/tmp \
		-v "$IMG:$IMG" \
		--device /dev/kvm \
		--device /dev/net/tun \
		--entrypoint= \
		"$@" \
		qemux/qemu-docker \
		qemu-system-x86_64 \
		-name "$HOSTNAME @$IPADDR" \
		-nodefaults \
		-serial stdio \
		-m 512M \
		-enable-kvm \
		-cpu host \
		-vga virtio \
		-display none \
		-fw_cfg name=opt/com.coreos/config,file="/tmp/$HOSTNAME.ign" \
		-device virtio-rng-pci \
		-netdev vde,id=qanet0,sock=/qa-switch/vde \
		-device "virtio-net,netdev=qanet0,mac=$MACADDR" \
		-drive "if=virtio,format=qcow2,file=/tmp/$HOSTNAME.qcow2"
}

runvm host1 10.0.0.2 52:54:00:12:34:56 10.0.0.3 --detach
runvm host2 10.0.0.3 52:54:00:12:34:57 10.0.0.2

podman rm --force qemu-tap-test-rootless-vde-switch || true
podman rm --force qemu-tap-test-rootless-vde-host1 || true
podman rm --force qemu-tap-test-rootless-vde-host2 || true
